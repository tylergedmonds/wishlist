package com.edmondsinc.wishlist;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WishlistApplication {

	public static void main(String[] args) {
		SpringApplication.run(WishlistApplication.class, args);
	}

//	@Bean
//	public ExtensionProvider<GeneratorConfiguration, ResolverBuilder> resolverBuilderExtensionProvider() {
//		String[] packages = {"com.edmondsinc.wishlist.config", "com.edmondsinc.wishlist.service"};
//		return (config, current) -> {
//			List<ResolverBuilder> resolverBuilders = new ArrayList<>();
//
//			//add a custom subtype of PublicResolverBuilder that only exposes a method if it's called "greeting"
//			resolverBuilders.add(new PublicResolverBuilder() {
//				@Override
//				public PublicResolverBuilder withBasePackages(String... basePackages) {
//					return super.withBasePackages(packages); //To change body of generated methods, choose Tools | Templates.
//				}
//
//			});
//			//add the default builder
//			resolverBuilders.add(new AnnotatedResolverBuilder().withBasePackages(packages));
//
//			return resolverBuilders;
//		};
//	}

}
