package com.edmondsinc.wishlist.config;

import lombok.Data;
import org.springframework.data.annotation.CreatedDate;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import java.time.LocalDateTime;
import java.util.UUID;

@Data
@MappedSuperclass
public abstract class AbstractEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id", nullable = false, unique = true)
    private Long id;

    @CreatedDate
    private LocalDateTime createdAt;

    @Column(name = "guid", nullable = false, unique = true, updatable = false)
    private UUID guid;

    protected AbstractEntity(){
        this.guid = UUID.randomUUID();
    }


}
