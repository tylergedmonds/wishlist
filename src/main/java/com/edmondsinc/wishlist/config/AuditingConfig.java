package com.edmondsinc.wishlist.config;

import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@EnableJpaAuditing
public class AuditingConfig {
}
