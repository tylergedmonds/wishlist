package com.edmondsinc.wishlist.model;

import com.edmondsinc.wishlist.config.AbstractEntity;
import com.edmondsinc.wishlist.model.dto.CreateUserDto;
import com.edmondsinc.wishlist.model.dto.UserDto;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Table;
import java.time.LocalDateTime;


@EqualsAndHashCode(callSuper = true)
@Entity
@Getter
@Setter
@Table(name = "user_account")
@Builder
@AllArgsConstructor
@EntityListeners(AuditingEntityListener.class)
public class User extends AbstractEntity {
    //AbstractAuditable<User, Long>

    String firstName;
    String lastName;
    String hashedPw;
    boolean active;
    @Column(nullable = false)
    String userName; //probably going to be email
    LocalDateTime lastLogin;

    public User(CreateUserDto u){
        this.firstName = u.getFirstName();
        this.lastName = u.getLastName();
        this.active = true; //when creating a user, we are defaulting the value to true.
        this.userName = u.getUserName();
    }

    public UserDto toUserDto(){
        UserDto userDto = new UserDto();
        userDto.setFirstName(this.firstName);
        userDto.setLastName(this.lastName);
        userDto.setActive(this.active);
        userDto.setUserName(this.userName);
        userDto.setLastLogin(this.getLastLogin());
        userDto.setUserGuid(this.getGuid());
        return userDto;
    }

    public User() {}

}
