package com.edmondsinc.wishlist.model.dto;

import lombok.Data;
import org.springframework.stereotype.Component;

@Data
@Component
public class CreateUserDto {
    String firstName;
    String lastName;
    String userName; //probably going to be email
    String unhashedPw;
}
