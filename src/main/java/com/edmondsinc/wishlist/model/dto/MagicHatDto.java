package com.edmondsinc.wishlist.model.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class MagicHatDto {
    PersonDto gifter;
    List<PersonDto> giftees;
}
