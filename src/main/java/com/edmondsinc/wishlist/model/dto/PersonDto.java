package com.edmondsinc.wishlist.model.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class PersonDto {
    String name;
    String email;           //intention to use this to email the person their matches directly.
    @JsonIgnore
    int matched;
    PersonDto forbiddenOne;

    public PersonDto(String name){
        this.name = name;
    }
}
