package com.edmondsinc.wishlist.model.dto;

import com.edmondsinc.wishlist.model.User;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;
import java.util.UUID;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class UserDto {
    String firstName;
    String lastName;
    boolean active;
    String userName; //probably going to be email
    LocalDateTime lastLogin;
    UUID userGuid;

    public UserDto(User user){
        this.firstName = user.getFirstName();
        this.lastName = user.getLastName();
        this.active = user.isActive();
        this.userName = user.getUserName();
        this.lastLogin = user.getLastLogin();
        this.userGuid = user.getGuid();
    }
}
