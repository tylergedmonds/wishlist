package com.edmondsinc.wishlist.model.dto;

import lombok.Data;

@Data
public class WishBankCreateDto {
    String name;
    Long userId;
}
