package com.edmondsinc.wishlist.model.dto;

import io.leangen.graphql.annotations.GraphQLInputField;
import lombok.Data;

@Data
public class WishCreateDto {
    Long wishBankId;
    String externalUrl;
    String notes;
    int qtyRequested;
    @GraphQLInputField(defaultValue = "false")
    boolean purchased;
    @GraphQLInputField(defaultValue = "true")
    boolean active;
}
