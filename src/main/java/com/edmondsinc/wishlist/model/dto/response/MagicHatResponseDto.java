package com.edmondsinc.wishlist.model.dto.response;

import com.edmondsinc.wishlist.model.dto.MagicHatDto;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import org.springframework.http.HttpStatus;

import java.util.List;


@Getter
@Setter
@AllArgsConstructor
public class MagicHatResponseDto extends ResponseBaseDto {
    List<MagicHatDto> magicHatDtoList;

    public MagicHatResponseDto(Integer httpCode, HttpStatus httpStatus, String message){
        super(httpCode, httpStatus, message);
    }
}
