package com.edmondsinc.wishlist.model.dto.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import org.springframework.http.HttpStatus;

@AllArgsConstructor
@Getter
@Setter
@Builder
public class ResponseBaseDto {
    private Integer httpCode;
    private HttpStatus httpStatus;
    private String message;

    public ResponseBaseDto(){
        this(200, HttpStatus.OK, "OK");
    }
}
