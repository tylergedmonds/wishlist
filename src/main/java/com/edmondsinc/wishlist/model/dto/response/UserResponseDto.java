package com.edmondsinc.wishlist.model.dto.response;

import com.edmondsinc.wishlist.model.dto.UserDto;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.stereotype.Component;

import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Component
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class UserResponseDto extends ResponseBaseDto {
    List<UserDto> userDtoList;
}
