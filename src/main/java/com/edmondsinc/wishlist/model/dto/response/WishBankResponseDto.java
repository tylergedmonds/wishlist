package com.edmondsinc.wishlist.model.dto.response;

import com.edmondsinc.wishlist.model.WishBank;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.http.HttpStatus;

import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Data
public class WishBankResponseDto extends ResponseBaseDto {
    Long id;
    String name;
    Long userId;
    List<WishResponseDto> wishList;

    public WishBankResponseDto(WishBank wb){
        this.id = wb.getId();
        this.name = wb.getBankName();
        this.userId = wb.getUser().getId();
    }

    public WishBankResponseDto(Integer httpCode, HttpStatus httpStatus, String message){
        super(httpCode, httpStatus, message);
    }
}
