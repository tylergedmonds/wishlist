package com.edmondsinc.wishlist.service;

import com.edmondsinc.wishlist.model.Wish;
import com.edmondsinc.wishlist.model.dto.WishCreateDto;
import com.edmondsinc.wishlist.model.dto.response.WishResponseDto;
import com.edmondsinc.wishlist.repository.WishBankRepo;
import com.edmondsinc.wishlist.repository.WishRepo;
import io.leangen.graphql.annotations.GraphQLMutation;
import io.leangen.graphql.annotations.GraphQLQuery;
import io.leangen.graphql.spqr.spring.annotations.GraphQLApi;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.NoResultException;
import java.util.Optional;

@Service
@GraphQLApi
@Slf4j
public class WishService {

    @Autowired
    WishRepo wishRepo;
    @Autowired
    WishBankRepo wishBankRepo;

    @GraphQLQuery
    public WishResponseDto getWishById(Long id){
        Optional<Wish> o = wishRepo.findById(id);
        if(o.isPresent()){
            return new WishResponseDto(o.get());
        }else{
            log.error("No result found for id {}", id);
            throw new NoResultException("No user found for id ["+id+"]");
        }
    }


    @GraphQLMutation
    public WishResponseDto addWish(WishCreateDto wishCreateDto){
        Wish wish = Wish.builder()
                .wishBank(wishBankRepo.getById(wishCreateDto.getWishBankId()))
                .active(true)
                .externalUrl(wishCreateDto.getExternalUrl())
                .notes(wishCreateDto.getNotes())
                .purchased(false)
                .qtyRequested(wishCreateDto.getQtyRequested())
                .build();
        wishRepo.save(wish);
        return new WishResponseDto(wish);
    }
}
