package com.edmondsinc.wishlist.model;

import com.edmondsinc.wishlist.repository.UserRepo;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;

@ExtendWith(SpringExtension.class)
@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
public class UserJpaTest {

    @Autowired
    UserRepo userRepo;

    @Test
    public void testCreateUser(){
        User user = User.builder().userName("testCase@tyler.com").firstName("Tyler").lastName("Edmonds").build();
        userRepo.save(user);
        List<User> users = userRepo.findAll();
        Assertions.assertThat(users).extracting(User::getUserName).contains("testCase@tyler.com");

        userRepo.deleteAll();
        Assertions.assertThat(userRepo.findAll()).isEmpty();
    }

}
